#!/usr/bin/env python
# -*- coding: cp1251 -*-
from pydc import *
import re
import json
import urllib, urllib2

class RedditBot(PyDC):
    address=''
    port=411
    debug=False
    nick = 'RedditBot'
    desc='Finds a popular reddit thread from links posted.'
    #password='123321'

    def onPublic(self, user, msg):
        print user, msg
        #s = re.search("(?P<url>https?://[^\s]+)", msg)
        #if s is not None: #If we've found a url in the post
        urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', msg)
        for link in urls:
            #link = s.group("url")
            print "Link found:", link
            api_url = "http://www.reddit.com/api/info.json?count=10&sort=hot&url="+urllib.quote_plus(link)

            hdr = { 'User-Agent' : 'link reverse search bot' }
            try:
                req = urllib2.Request(api_url, headers=hdr)
                reddit_json = urllib2.urlopen(req).read()

                posts = json.loads(reddit_json)["data"]["children"]
                
                if len(posts) > 0:
                    print len(posts), "posts found"
                    max_comments = 0
                    best_post = None
                    for post in posts:
                        num_comments = post["data"]["num_comments"]
                        if int(num_comments) > max_comments:
                            max_comments = int(num_comments)
                            best_post = post
                        #print post["data"]["title"], num_comments
                    if best_post is not None and max_comments > 200: #arbitrary number
                        post_link = best_post["data"]["title"]+ ' -> http://www.reddit.com'+best_post["data"]["permalink"]
                        print post_link
                        self.say(post_link)
            except urllib2.HTTPError:
                print "HTTPError"


##############################################
#      Start the bot...
##############################################
bot=RedditBot()
print 'Connecting to dc hub ' + bot.address + ' using port '+str(bot.port)
bot.connect()