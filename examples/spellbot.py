#!/usr/bin/env python
# -*- coding: cp1251 -*-
from pydc import *

import enchant

class SpellBot(PyDC):
    address=''
    port=411
    debug=False
    nick = 'SpellBot'
    desc='I cun spall gerd.'
    #password='123321'

    def onPublic(self, user, msg):
        print (user, msg)
        d = enchant.Dict("en_GB")
        words = msg.split()
        output = ''
        respond = False
        for word in words:
            if not d.check(word):
                suggestions = d.suggest(word)
                if len(suggestions) > 0:
                    output += suggestions[0] + ' '
                    print (d.suggest(word))
                else:
                    output += '[ERROR] '
                respond = True
            else:
                output += word + ' '
        if respond:
            self.say(output)

##############################################
#      Start the bot...
##############################################
bot=SpellBot()
print ('Connecting to dc hub', bot.address, 'using port', str(bot.port))
bot.connect()